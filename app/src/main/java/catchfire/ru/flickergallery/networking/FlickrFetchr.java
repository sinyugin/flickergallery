package catchfire.ru.flickergallery.networking;

import android.net.Uri;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import catchfire.ru.flickergallery.model.GalleryItem;

/**
 * Class handle the networking in this app
 * HttpURLConnection represents a connection, but it will not actually connect to your endpoint
 * until you call getInputStream() (or getOutputStream()
 * Key: 0a8cc73b6f5489061cb09f167ea4d0dd
 * Secret: a899bf637c774d7a
 */

public class FlickrFetchr {

    public static final String TAG = "FlickrFetchr";
    public static final String API_KEY = "0a8cc73b6f5489061cb09f167ea4d0dd";

    private static final String FETCH_RECENTS_METHOD = "flickr.photos.getRecent";
    private static final String SEARCH_METHOD = "flickr.photos.search";

    private static final Uri ENDPOINT = Uri.parse("https://api.flickr.com/services/rest/")
            .buildUpon()
            .appendQueryParameter("api_key", API_KEY)
            .appendQueryParameter("format", "json")
            .appendQueryParameter("nojsoncallback","1")
            .appendQueryParameter("extras", "url_s")
            .build();


    private String buildUrl(String method, String query){
        Uri.Builder uriBuilder = ENDPOINT.buildUpon().appendQueryParameter("method", method);
        if (method.equals(SEARCH_METHOD)){
            uriBuilder.appendQueryParameter("text", query);
        }
        return uriBuilder.build().toString();
    }

    public byte[] getUrlBytes(String urlSpec) throws IOException {

        URL url = new URL(urlSpec);
        HttpURLConnection connection = (HttpURLConnection) url.openConnection();

        try {
            ByteArrayOutputStream out = new ByteArrayOutputStream();
            InputStream is = connection.getInputStream();

            if (connection.getResponseCode() != HttpURLConnection.HTTP_OK) {
                throw new IOException(connection.getResponseMessage() + " with " + urlSpec);
            }

            int bytesRead = 0;
            byte[] buffer = new byte[1024];

            while ((bytesRead = is.read(buffer)) > 0){
                out.write(buffer, 0, bytesRead);
                //Log.d(TAG, buffer.toString() + " " + is.read(buffer));
            }

            out.close();
            return out.toByteArray();
        } finally {
            connection.disconnect();
        }
    }

    public String getUrlString(String urlSpec) throws IOException {
        return new String(getUrlBytes(urlSpec));
    }

    private void parseItem(List<GalleryItem> items, JSONObject object) throws JSONException {
        JSONObject photos = object.getJSONObject("photos");
        JSONArray array = photos.getJSONArray("photo");

        for (int i = 0; i < array.length(); i++){
            JSONObject photo = array.getJSONObject(i);

            GalleryItem item = new GalleryItem();

            if (!photo.has("url_s"))
                continue;

            item.setId(photo.getString("id"));
            item.setCaption(photo.getString("title"));
            item.setUrl(photo.getString("url_s"));
            item.setOwner(photo.getString("owner"));

            items.add(item);
        }
    }

    private List<GalleryItem> downloadGalleryItems(String url) {
        List<GalleryItem> list = new ArrayList<>();

        try {
            String jsonString = getUrlString(url);
            Log.i(TAG, "Received JSON: " + jsonString);

            JSONObject jsonBody = new JSONObject(jsonString);
            parseItem(list, jsonBody);

        } catch (IOException ioe) {
            Log.e(TAG, "Failed to fetch items", ioe);
        } catch (JSONException je) {
            Log.e(TAG, "Failed to parse JSON", je);
        }

        return list;
    }

    public List<GalleryItem> fetchRecentPhotos(){
        String url = buildUrl(FETCH_RECENTS_METHOD, null);
        Log.i(TAG, "" + url);
        return downloadGalleryItems(url);
    }

    public List<GalleryItem> searchPhotos(String query){
        String url = buildUrl(SEARCH_METHOD, query);
        return downloadGalleryItems(url);
    }


}
