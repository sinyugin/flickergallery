package catchfire.ru.flickergallery.service;

import android.app.Activity;
import android.app.AlarmManager;
import android.app.IntentService;
import android.app.Notification;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.net.ConnectivityManager;
import android.os.SystemClock;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import java.util.List;
import java.util.concurrent.TimeUnit;

import catchfire.ru.flickergallery.PhotoGalleryActivity;
import catchfire.ru.flickergallery.R;
import catchfire.ru.flickergallery.model.GalleryItem;
import catchfire.ru.flickergallery.networking.FlickrFetchr;
import catchfire.ru.flickergallery.utils.QueryPreferences;



public class PollService extends IntentService {

    private static final String TAG = "PollService";
    private static final long POLL_INTERVAL_MS = TimeUnit.MINUTES.toMillis(1);
    public static final String ACTION_SHOW_NOTIFICATION = "ru.catchfire.flickergallery.service.pollservice.SHOW_NOTIFICATION";
    public static final String PERM_PRIVATE = "ru.catchfire.flickergallery.service.pollservice.PRIVATE";

    public static final String REQUEST_CODE = "REQUEST_CODE";
    public static final String NOTIFICATION = "NOTIFICATION";

    public static Intent newIntent(Context context){
        return new Intent(context, PollService.class);
    }

    public PollService() {
        super(TAG);
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        Log.i(TAG, "Received an intent: " + intent);

        if (!isNetworkAvailableAndConnectes()){
            return;
        }
        hasNewQueryRespond();

        Resources resources = getResources();
        Intent i = PhotoGalleryActivity.newIntent(this);
        PendingIntent pi = PendingIntent.getActivity(this, 0, i, 0);

        Notification notification = new NotificationCompat.Builder(this)
                .setTicker(resources.getString(R.string.new_pictures_title))
                .setSmallIcon(android.R.drawable.ic_menu_report_image)
                .setContentTitle(resources.getString(R.string.new_pictures_title))
                .setContentText(resources.getString(R.string.new_pictures_text))
                .setContentIntent(pi)
                .setAutoCancel(true)
                .build();

        //NotificationManagerCompat man = NotificationManagerCompat.from(this);
        //man.notify(0, notification);
        // отправляем внутренний интент для выключения notification когда приложение работает
        // sendBroadcast(new Intent(ACTION_SHOW_NOTIFICATION), PERM_PRIVATE);
        showBackgroundNotification(0, notification);
    }

    private void showBackgroundNotification(int requestCode, Notification notification) {
        Intent i = new Intent(ACTION_SHOW_NOTIFICATION);
        i.putExtra(NOTIFICATION, notification);
        i.putExtra(REQUEST_CODE, requestCode);
        sendOrderedBroadcast(i, PERM_PRIVATE, null, null, Activity.RESULT_OK, null, null);
    }

    private boolean isNetworkAvailableAndConnectes() {
        ConnectivityManager cm = (ConnectivityManager) getSystemService(CONNECTIVITY_SERVICE);
        boolean isNetworkAvailable = cm.getActiveNetworkInfo() != null;
        return isNetworkAvailable && cm.getActiveNetworkInfo().isConnected();
    }

    // Метод проверяет появились ли новые результаты для query
    private void hasNewQueryRespond() {
        String query = QueryPreferences.getStoredQuery(this);
        String lastResultId = QueryPreferences.getPrefLastResultId(this);

        List<GalleryItem> items;

        if (query == null){
            items = new FlickrFetchr().fetchRecentPhotos();
        } else {
            items = new FlickrFetchr().searchPhotos(query);
        }

        if (items.size() == 0){
            return;
        }

        String resultId = items.get(0).getId();
        if (resultId.equals(lastResultId)){
            Log.i(TAG, "Got an old result: " + resultId);
        } else {
            Log.i(TAG, "Got a new result: " + resultId);
        }
        QueryPreferences.setPrefLastResultId(this, resultId);
    }


    // В зависимости значения параметра "isOn" осуществляется запуск сервиса по расписанию
    //PendindIntent - "ожидаемый intent"
    public static void setServiceAlarm(Context context, boolean isOn){
        // подготавливаем сервис для запуска
        Intent i = PollService.newIntent(context);

        // запускаем сервис
        PendingIntent pi = PendingIntent.getService(context, 0, i, 0);
        AlarmManager am = (AlarmManager) context.getSystemService(ALARM_SERVICE);

        // установка запуска по расписанию или отмена расписания
        if (isOn){
            am.setRepeating(AlarmManager.ELAPSED_REALTIME,
                    SystemClock.elapsedRealtime() ,
                    POLL_INTERVAL_MS,
                    pi);
            Log.i(TAG, "Started polling");
        } else {
            am.cancel(pi);
            pi.cancel();
            Log.i(TAG, "Stopped polling");
        }

        //записываем включен ли Аларм для запуска broadcastreceiver
        QueryPreferences.setAlarmOn(context, isOn);
    }

    // Проверяет включен Alarm. Если результат false - значит выключен.
    public static boolean isServiceAlarmOn(Context context){
        Intent i = PollService.newIntent(context);
        PendingIntent pi = PendingIntent.getService(context, 0, i, PendingIntent.FLAG_NO_CREATE);
        return pi != null;
    }

}
