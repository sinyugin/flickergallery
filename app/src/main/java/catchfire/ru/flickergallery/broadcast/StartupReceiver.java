package catchfire.ru.flickergallery.broadcast;

import android.content.Context;
import android.content.Intent;
import android.content.BroadcastReceiver;
import catchfire.ru.flickergallery.service.PollService;
import catchfire.ru.flickergallery.utils.QueryPreferences;

/**
 * Standalone receiver
 */

public class StartupReceiver extends BroadcastReceiver {

    private static final String TAG = "StartupReceiver";

    @Override
    public void onReceive(Context context, Intent intent) {
        boolean isOn = QueryPreferences.isAlarmOn(context);
        PollService.setServiceAlarm(context, isOn);
    }
}
